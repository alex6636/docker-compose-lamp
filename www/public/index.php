<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>LAMP</title>
    </head>
    <body>
        <img style="max-width:600px" src="https://i.imgur.com/YX2nNw2.png"/>
        <h1>LAMP</h1>
        <h2><?= apache_get_version(); ?></h2>
        <h2>PHP <?= phpversion(); ?></h2>
        <h2>
        <?php
        $link = mysqli_connect("mysql", "root", "root", null);
        if (mysqli_connect_errno()) {
            printf("MySQL connecttion failed: %s", mysqli_connect_error());
        } else {
            /* print server version */
            printf("MySQL Server %s successfully connected", mysqli_get_server_info($link));
        }
        /* close connection */
        mysqli_close($link);
        ?>
        </h2>
        <p>
            <a target="_blank" href="http://localhost/pi.php">phpinfo()</a> |
            <a target="_blank" href="http://localhost:8080">phpMyAdmin</a> |
            <a target="_blank" href="http://localhost/test_db.php">Test DB Connection</a> |
            <a target="_blank" href="http://localhost/test_composer.php">Test Composer</a> |
            <a target="_blank" href="http://localhost/test-mod-rewrite-ok">Test Mod Rewrite <?=($_SERVER['REQUEST_URI'] === '/test-mod-rewrite-ok') ? '(OK!)' : ''?></a>
        </p>
    </body>
</html>