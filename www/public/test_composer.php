<?php
if (!file_exists('../vendor/autoload.php')) {
    echo 'Run following commands:<br/><br/>';
    echo 'docker exec -it alexs-webserver /bin/bash<br/>';
    echo 'cd /var/www<br/>';
    echo 'composer update';
    exit;
}

require '../vendor/autoload.php';
echo 'Composer OK!';