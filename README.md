![title](https://i.imgur.com/YX2nNw2.png)

# LAMP stack built with Docker Compose

* PHP
* Apache
* MySQL
* phpMyAdmin

## Installation


```shell
git clone https://createtruesite@bitbucket.org/alex6636/docker-compose-lamp.git
```

Your LAMP stack is now ready!! You can access it via `http://localhost`.
phpMyAdmin is `http://localhost:8080`

## Composer

```shell
docker exec -it alexs-webserver /bin/bash
composer update
```